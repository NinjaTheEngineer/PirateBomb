# PEC_FINAL_PirateBomb

## Game Description
The project is a platformer game played by levels called Pirate Bomb. The game primary mechanic is the usage of bombs, that can interact with each other and that the enemy can interact with as well. The players must defeat the enemies with the help of their bombs to get to the end of the level.

![PirateBomb](https://i.imgur.com/RkJDrs1.png)

## Project Implementations:
- There's a title screen that leads to a main menu, both screens with their animations.
- There's two levels, one tutorial and one easy level.
- There's a credits scene after the second level.
- The players have the option to change all the keybinds, increasing the game's accessibility and usability.
- The game implements a state machine for the enemy, making it easy to scale on and create new enemies from them.
- There's implemented a cinemachine camera for camera shake effects and a smoother gameplay feeling.
- The levels were created using grids and tilemaps.
- There's a sound manager implemented for the background musics and sound effects.
- There's a level loader that smoothers the scene transitions.
- There's a 'Monkay Studios' logo in the beginning of the game.

## Resources & Imports
All imports used in game were either with a free license or free imports from Unity.

Sprites are from a pixel artist named Pixel Frog: https://pixelfrog-assets.itch.io/pirate-bomb
Sounds imported from: https://freesound.org/
Music imported from: https://www.chosic.com/
cinemachine, TMPro from Unity.

UI Animations created with Unity Engine.

# Structure
The game has 4 different scenes, the first one being the StartScene that handles the title screen and main menu, in the main menu the users can start to play the game or use the options menu to customize their controls, this scene is controlled by a StartSceneManager.

There's a Level_Tutorial and a Level_1 scenes that are meant to be the gameplay scenes, both use a GameManager to handle all gameplay oriented logic like starting the game and ending it, the Level_Tutorial also uses a TutorialManager to personalize the tutorial texts presented and initialize the enemy for the tutorial.

After the Level_1 scene the game will transition to a credits scene presenting the end of the game. This scene only uses an animation to pass the credtis while there's a CreditsManager to change the BackgroundMusic to the CreditsMusic.

## Game Manager
The game manager is the core manager of the gameplay and handles the game's initialization and ending, receiving the game's information from other scripts like the PlayerController that tells the GameManager that the player has lost all lives and therefore the GameManager communicates to the UIManager that the game is over.

## Player
The player uses a PlayerController script for all the player's platforming controls, collisions and bomb usage.

## Enemy
The enemy is controlled by a finite state machine that handles all it's behaviours.

## Keybinds
The KeybindManager saved the keybinds changed by the player and implements them in the PlayerController and TutorialManager.

## Finite State Machine
The state machine has as many states as needed and is easily expandable, at the moment the enemy uses 9 different states for idleing, moving, getting stunned, melee attacking, looking for targets, detecting the player, chasing the player, dying and a tutorial state just for the tutorial scene. These states are then overrided for the especific enemy and worked upon in their new class. Leaving the base state class with only the common behaviour that can be inherited by other new future enemies.

## Bomb
The bomb has a BombController that handles its detonation, collisions and knockbacks.

## SoundManager
The sound manager is implemented from any script that where a sound can be fit in, either for the enemie, the player, the bomb, or the UI.

## Tags and Layers
There were created different tags like Enemy, Bomb, Object, Keybind, ExtiDoor and VolumeSlider for finding objects and collision detections. There was also created different layers like Damagable, Ground, Player, Bomb, Object, Push and ExitDoor for collision detections. And it was created different sorting layer like Background, Objects, Light, Foreground, Enemy, Player, Bomb, Effect and ExitDoor to layer the objects in the correct 

## Demo
https://vimeo.com/665794047
