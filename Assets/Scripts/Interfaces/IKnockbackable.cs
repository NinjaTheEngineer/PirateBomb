﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//For any object thaht receives knockback from another object
public interface IKnockbackable {
    void Knockback(float knockbackStrength, Vector2 knockbackPosition);
}