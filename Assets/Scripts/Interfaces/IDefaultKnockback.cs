﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//For any object that receives knockback
public interface IDefaultKnockback {
    void Knockback(int facingDirection);
}
