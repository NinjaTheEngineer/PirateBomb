﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//For any damageable object
public interface IDamageable {
    void Damage(int amount);
}
