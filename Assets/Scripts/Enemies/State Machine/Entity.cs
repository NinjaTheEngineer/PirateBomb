﻿using System.Collections;
using UnityEngine;
using NinjaTools;
//Class for any enemy
public class Entity : NinjaMonoBehaviour, IKnockbackable, IDamageable {
    public FiniteStateMachine stateMachine;
    public Core Core { get; private set; }
    public D_Entity entityData;
    public int currentHealth;
    public int facingDirection { get; private set; }
    public bool isRotating { get; set; }
    public bool isPlayerAlive { get; set; }
    public Rigidbody2D rb { get; private set; }
    public Animator anim { get; private set; }
    public GameObject visualGO { get; private set; }
    public AnimationToStateMachine atsm { get; private set; }

    [SerializeField]
    private Transform wallCheck;

    [SerializeField]
    private Transform groundCheck;

    [SerializeField]
    private Transform floorLedgeCheck;

    [SerializeField]
    private Transform upperLedgeCheck;

    [SerializeField]
    private Transform playerCheck;

    [SerializeField]
    private Transform playerJumpAboveCheck;

    [SerializeField]
    private Transform interrogationEffects;

    private bool isInTheAir;

    private Vector3 interrogationEffectsStartingPosition;
    private Vector2 velocityWorkspace;

    //Initialize Core and FSM
    public virtual void Awake() {
        Core = GetComponentInChildren<Core>();
        stateMachine = new FiniteStateMachine();
    }
    //Initialize variables and effects
    public virtual void Start() {
        isPlayerAlive = true;
        EventManager.OnPlayerDeath += PlayerIsDead;
        interrogationEffectsStartingPosition = interrogationEffects.localPosition;
        SetDetectingTargetEffects(false);

        isRotating = false;

        currentHealth = entityData.maxHealthAmount;
        visualGO = transform.Find("Visual").gameObject;
        rb = GetComponent<Rigidbody2D>();
        anim = visualGO.GetComponent<Animator>();
        atsm = visualGO.GetComponent<AnimationToStateMachine>();

    }
    //Update Logic
    public virtual void Update() {
        Core.LogicUpdate();
        stateMachine.currentState.LogicUpdate();
        UpdateAnimations();
    }
    //Updates animations 
    private void UpdateAnimations() {
        isInTheAir = CheckIfInTheAir();
        anim.SetBool("isGrounded", Core.CollisionSenses.Ground && !isInTheAir);
        anim.SetBool("isPlayerAlive", isPlayerAlive);
    }
    //Physics update
    public virtual void FixedUpdate() {
        stateMachine.currentState.PhysicsUpdate();
    }
    //Sets velocity to the entity
    public virtual void SetVelocity(float velocity) {
        velocityWorkspace.Set(facingDirection * velocity, rb.velocity.y);
        rb.velocity = velocityWorkspace;
    }
    //Check if the player jumped above
    public virtual bool CheckPlayerJumpAbove() {
        return Physics2D.Raycast(playerJumpAboveCheck.position, Vector2.up, entityData.playerJumpAboveDistance, entityData.whatIsPlayer);
    }
    //Check if player in min distance to do something
    public virtual bool CheckPlayerInMinAgroRange() {
        if (Physics2D.Raycast(playerCheck.position, visualGO.transform.right, entityData.minAgroDistance, entityData.whatIsPlayer) && isPlayerAlive) {
            float distance = Physics2D.Raycast(playerCheck.position, visualGO.transform.right, entityData.minAgroDistance, entityData.whatIsPlayer).distance;
            return !Core.CollisionSenses.WallBeforePlayer(distance);
        } else {
            return false;
        }
    }
    //Check if palyer in max distance to do something
    public virtual bool CheckPlayerInMaxAgroRange() {
        return (Physics2D.CircleCast(playerCheck.position, entityData.maxAgroDistance, visualGO.transform.right, entityData.whatIsPlayer) && isPlayerAlive);
    }
    //Check if bomb in min distance to do something
    public virtual bool CheckBombInMinAgroRange() {
        return (Physics2D.Raycast(playerCheck.position, visualGO.transform.right, entityData.minAgroDistance, entityData.whatIsBomb));
    }
    //Check if bomb in max distance to do something
    public virtual bool CheckBombInMaxAgroRange() {
        return (Physics2D.CircleCast(playerCheck.position, entityData.bombMaxAgroRange, visualGO.transform.right, entityData.whatIsBomb));
    }
    //Check if player in range for close action
    public virtual bool CheckPlayerInCloseRangeAction() {
        return Physics2D.Raycast(playerCheck.position, visualGO.transform.right, entityData.closeRangeActionDistance, entityData.whatIsPlayer);
    }
    //Check if bomb in range for close action
    public virtual bool CheckBombInCloseRangeAction() {
        return Physics2D.Raycast(playerCheck.position, visualGO.transform.right, entityData.closeRangeActionDistance, entityData.whatIsBomb);
    }
    //Check if isn't grounded
    public virtual bool CheckIfInTheAir() {
        return !(Core.Movement.CurrentVelocity.y <= 0.005f && Core.Movement.CurrentVelocity.y >= -0.005f);
    }
    //Set detecting target effects
    public virtual void SetDetectingTargetEffects(bool active) {
        if (active) {
            SoundManager.Instance.PlayPirateHm();
        }
        interrogationEffects.gameObject.SetActive(active);

        if (Core.Movement.FacingDirection.Equals(1)) {
            interrogationEffects.localPosition = interrogationEffectsStartingPosition;
        } else {
            interrogationEffects.localPosition = new Vector3(-interrogationEffectsStartingPosition.x, interrogationEffectsStartingPosition.y, 0f);
        }
    }
    //Jump
    public virtual void Jump() {
        rb.velocity = new Vector2(rb.velocity.x, 6.25f);
    }
    //Flip character
    public virtual void Flip() {
        Core.Movement.Flip();
        HandleEffectsPositionOnFlip();
    }
    //Rotate enemy
    public virtual void RotateEnemy(float chaseSpeed) {
        StartCoroutine(RotateEnemyRoutine(chaseSpeed));
    }
    //Rotate after time
    IEnumerator RotateEnemyRoutine(float chaseSpeed) {
        var logId = "RotateEnemyRoutine";
        yield return new WaitForSecondsRealtime(0.35f);
        logd(logId, "Rotate Enemy");
        Flip();
        isRotating = false;
        Core.Movement.SetVelocity(chaseSpeed, facingDirection);
    }
    //Handle the position of the detecting effect
    private void HandleEffectsPositionOnFlip() {
        interrogationEffects.localPosition = new Vector3(-interrogationEffects.localPosition.x, interrogationEffects.localPosition.y, 0f);
        interrogationEffects.Rotate(0f, 180f, 0f);
    }
    //Debug purpose
    public virtual void OnDrawGizmos() {
        Gizmos.DrawLine(wallCheck.position, wallCheck.position + (Vector3)(Vector2.right * facingDirection * entityData.wallCheckDistance));

        Gizmos.DrawLine(groundCheck.position, groundCheck.position + (Vector3)(Vector2.down * entityData.groundCheckDistance));

        Gizmos.DrawLine(upperLedgeCheck.position, upperLedgeCheck.position + (Vector3)(Vector2.right * facingDirection * entityData.upperLedgeCheckDistance));

        Gizmos.DrawLine(floorLedgeCheck.position, floorLedgeCheck.position + (Vector3)(Vector2.down * entityData.floorLedgeCheckDistance));

        Gizmos.DrawWireSphere(playerCheck.position + (Vector3)(Vector2.right * entityData.closeRangeActionDistance), 0.2f);
        Gizmos.DrawWireSphere(playerCheck.position + (Vector3)(Vector2.right * entityData.minAgroDistance), 0.2f);
        Gizmos.DrawWireSphere(playerCheck.position + (Vector3)(Vector2.right), entityData.maxAgroDistance);
    }
    //Damge entity
    public virtual void Damage(int amount) {
        currentHealth -= amount;
    }
    //Handle entity knockback
    public virtual void Knockback(float knockbackStrength, Vector2 bombPosition) {
        Core.Combat.ApplyKnockback(knockbackStrength, bombPosition);
    }
    //Set player dead
    public void PlayerIsDead() {
        isPlayerAlive = false;
        EventManager.OnPlayerDeath -= PlayerIsDead;
    }
}
