﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiniteStateMachine {
    //global get, private set
    public State currentState { get; private set; }
    //Initialize state machine
    public void Initialize(State startingState) {
        currentState = startingState;
        currentState.Enter();
    }
    //Change any state
    public void ChangeState(State newState) {
        currentState.Exit();
        currentState = newState;
        currentState.Enter();
    }
}
