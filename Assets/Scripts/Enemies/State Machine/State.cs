﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State {
    protected Core core;

    protected FiniteStateMachine stateMachine;
    protected Entity entity;

    protected float startTime;
    protected float lastRotateTime;

    protected string animBoolName;
    //State contructor

    public State(Entity entity, FiniteStateMachine stateMachine, string animBoolName) {
        this.entity = entity;
        this.stateMachine = stateMachine;
        this.animBoolName = animBoolName;
        core = entity.Core;
    }
    //Like a state Start() method
    public virtual void Enter() {
        startTime = Time.time;
        lastRotateTime = startTime;
        entity.anim.SetBool(animBoolName, true);
        DoChecks();
    }
    //Exit method 
    public virtual void Exit() {
        entity.anim.SetBool(animBoolName, false);
    }
    //Update method, logic update
    public virtual void LogicUpdate() {

    }
    //Physics update
    public virtual void PhysicsUpdate() {
        DoChecks();
    }

    //Do Physics checks
    public virtual void DoChecks() {

    }
}
