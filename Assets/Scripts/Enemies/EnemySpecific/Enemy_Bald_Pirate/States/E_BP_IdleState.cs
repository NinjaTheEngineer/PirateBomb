﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E_BP_IdleState : IdleState {
    private Enemy_Bald_Pirate enemy;
    public E_BP_IdleState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_IdleState stateData, Enemy_Bald_Pirate enemy) : base(entity, stateMachine, animBoolName, stateData) {
        this.enemy = enemy;
    }

    public override void Enter() {
        base.Enter();
    }

    public override void Exit() {
        base.Exit();
    }

    public override void LogicUpdate() {
        base.LogicUpdate();

        if (isPlayerInMinAgroRange || isBombInMinAgroRange) {
            stateMachine.ChangeState(enemy.playerDetectedState);
        }

        if (isIdleTimeOver) {
            stateMachine.ChangeState(enemy.moveState);
        }
    }

    public override void PhysicsUpdate() {
        base.PhysicsUpdate();
    }
}
