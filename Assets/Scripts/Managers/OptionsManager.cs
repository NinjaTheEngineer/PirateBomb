using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class OptionsManager : MonoBehaviour {
    private static OptionsManager instance;
    private GameObject[] keybindButtons;
    //Singleton
    public static OptionsManager Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<OptionsManager>();
            }
            return instance;
        }
    }
    //Get keybinds buttons
    private void Awake() {
        keybindButtons = GameObject.FindGameObjectsWithTag("Keybind");
    }
    //Update keybind text
    public void UpdateKeyText(string key, KeyCode keyCode) {
        TextMeshProUGUI tmp = Array.Find(keybindButtons, x => x.name == key).GetComponentInChildren<TextMeshProUGUI>();
        tmp.text = keyCode.ToString();
    }
    //Update keybind text while binding new key
    public void KeyBindOnClick(string key) {
        TextMeshProUGUI tmp = Array.Find(keybindButtons, x => x.name == key).GetComponentInChildren<TextMeshProUGUI>();
        tmp.text = "----";
    }
}
