using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public bool GameStarted { get; private set; }
    public GameObject player;

    [SerializeField]
    private bool isTutorial;

    public bool IsTutorial() { return isTutorial; }

    private bool openingMenu;
    public GameObject optionsMenu;
    public GameObject optionsPanel;
    public GameObject door;
    private static GameManager instance;
    //Singleton
    public static GameManager Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<GameManager>();
            }
            return instance;
        }
    }
    private void Awake() {
        GameStarted = false;
    }
    //Initialize entering level
    private void Start() {
        player.SetActive(true);
        StartCoroutine(EnterLevel());
    }
    //Handle enter level animations
    private IEnumerator EnterLevel() {
        yield return new WaitForSeconds(1.5f);
        SoundManager.Instance.PlayDoorOpen();
        door.GetComponent<Animator>().SetBool("open", true);
        player.GetComponent<Animator>().SetTrigger("doorOut");
        yield return new WaitForSeconds(0.5f);
        GameStarted = true;
    }
    //Go to main menu
    public void OpenMainMenu() {
        SoundManager.Instance.PlayButtonClick();
        CloseOptionsMenu();
        LevelLoader.Instance.LoadMainMenu();
    }
    //Restart level
    public void RestartLevel() {
        SoundManager.Instance.PlayButtonClick();
        CloseOptionsMenu();
        LevelLoader.Instance.RestartLevel();
    }
    //Set game over
    public void GameOver() {
        GameStarted = false;
        OpenOptionsMenu();
        UIManager.Instance.UpdateGameOver();
    }
    //Open options menu in game
    public void OpenOptionsMenu() {
        openingMenu = true;
        optionsMenu.SetActive(true);
        optionsPanel.SetActive(true);
        CanvasRenderer canvas = optionsPanel.GetComponent<CanvasRenderer>();
        canvas.SetAlpha(0f);

        StartCoroutine(LerpPosition(optionsMenu, new Vector2(optionsMenu.transform.position.x, 525f), 0.35f));
        StartCoroutine(LerpFunction(canvas, 0.66f, 0.33f));
    }
    //Close options menu
    public void CloseOptionsMenu() {
        SoundManager.Instance.PlayButtonClick();
        openingMenu = false;
        StartCoroutine(DeactivateOptionsMenu());
        StartCoroutine(DeactivateOptionsBackground());
        StartCoroutine(LerpPosition(optionsMenu, new Vector2(optionsMenu.transform.position.x, -525f), 0.35f));
        StartCoroutine(LerpFunction(optionsPanel.GetComponent<CanvasRenderer>(), 0f, 0.33f));
    }
    //Lerp function for the options menu transition
    IEnumerator LerpPosition(GameObject objectMoving, Vector2 targetPosition, float duration) {
        Time.timeScale = 1f;
        float time = 0;
        Vector2 startPosition = objectMoving.transform.position;

        while (time < duration) {
            objectMoving.transform.position = Vector2.Lerp(startPosition, targetPosition, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        objectMoving.transform.position = targetPosition;
        if (openingMenu) {
            Time.timeScale = 0f;
        }
    }
    //Lerp for the fade of the options background
    IEnumerator LerpFunction(CanvasRenderer elementToFade, float endValue, float duration) {
        float time = 0;
        float startValue = elementToFade.GetAlpha();

        while (time < duration) {
            elementToFade.SetAlpha(Mathf.Lerp(startValue, endValue, time / duration));

            time += Time.deltaTime;
            yield return null;
        }
        elementToFade.SetAlpha(endValue);
    }
    //Deactivates background
    private IEnumerator DeactivateOptionsBackground() {
        yield return new WaitForSeconds(0.4f);
        optionsPanel.SetActive(false);
    }

    //Deactivates options menu
    private IEnumerator DeactivateOptionsMenu() {
        yield return new WaitForSeconds(0.4f);
        optionsMenu.SetActive(false);
    }

}
