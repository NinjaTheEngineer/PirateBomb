using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static System.Net.Mime.MediaTypeNames;

public class UIManager : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI titleText;

    [SerializeField] private GameObject volumeSettings;
    [SerializeField] private GameObject gameOverText;

    [SerializeField] private Animator heart_1, heart_2, heart_3;
    [SerializeField] private UnityEngine.UI.Image bombIcon;
    [SerializeField] private TextMeshProUGUI bombAmount;


    private static UIManager instance;
    //Singleton
    public static UIManager Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<UIManager>();
            }
            return instance;
        }
    }
    //Initialize singleton
    private void Awake() {
        instance = this;
    }
    //Update players health UI
    public void UpdatePlayerHealth(int healthAmount) {
        switch (healthAmount) {
            case 2:
                heart_3.SetTrigger("loseHeart");
                break;
            case 1:
                heart_2.SetTrigger("loseHeart");
                break;
            case 0:
                heart_1.SetTrigger("loseHeart");
                break;
        }
    }
    //Update bomb amount UI 
    public void UpdateBombAmount(int bombsAmount) {
        bombAmount.SetText("x" + bombsAmount.ToString());
    }
    //Update bomb reload UI
    public void UpdateBombReloadTime(float time) {
        bombIcon.fillAmount = time;
    }
    //Handle game over dialog
    public void UpdateGameOver() {
        titleText.text = "Defeated";
        volumeSettings.SetActive(false);
        gameOverText.SetActive(true);
    }

}
