﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {
    public delegate void PlayerDeath();
    public static event PlayerDeath OnPlayerDeath;
    //Tells every enemy that the player is dead
    public void BroadCastPlayerDeath() {
        if (OnPlayerDeath != null) {
            OnPlayerDeath();
        }
    }
}
