﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Diagnostics;
using NinjaTools;
public class PlayerController : NinjaMonoBehaviour, IKnockbackable, IDamageable, IDefaultKnockback {
    #region Variables

    public TextMeshProUGUI healthText;

    private int amountOfJumpsLeft;
    private int amountOfBombsLeft;
    private int healthAmount;

    private float bombsRefreshTimeLeft;
    private float holdDownStartTime;
    private float holdDownTime = 0f;
    private float walkTime = 0f;
    private float walkTimeStarted;
    private float walkEffectCd = 0.5f;
    private float knockbackStartTime;
    private float movementInputDirection;

    private bool holdingBomb = false;
    private bool isFacingRight = true;
    private bool isWalking = false;
    private bool isGrounded = false;
    private bool isJumping = false;
    private bool canJump = false;
    private bool isKnockbackActive = false;
    private bool canMove = true;
    private bool isHit = false;
    private bool isAlive = true;
    private bool isAtExitDoor = false;
    private bool enteredLevel = false;
    private bool exitingLevel = false;

    private Vector2 rbVelocity;
    private Animator exitDoor;

    private Rigidbody2D rb;
    private Animator anim;

    public int amountOfJumps;
    public int amountOfBombs;

    public int healthAmountMax = 3;

    public float bombsRefreshTime = 1.25f;
    public float minHoldDownTime = 0.25f;
    public float maxHoldDownTime = 2.5f;
    public float maxBombForce = 2f;

    public float movementSpeed = 5.0f;
    public float jumpForce = 16.0f;
    public float groundCheckRadius = 0.125f;
    public float exitDoorCheckRadius = 0.05f;
    public float variableJumpHeightMultiplier = 0.5f;

    public float knockbackDuration = 0.75f;
    public float xKnockbackMultiplier = 1f;
    public float yKnockbackMultiplier = 1f;

    public Transform groundCheck;
    public Transform exitDoorCheck;
    public Transform bombPlacement;
    public Transform jumpEffectPosition;
    public Transform fallEffectPosition;
    public Transform runEffectPosition;
    public HoldBarScript holdBarObject;

    public GameObject bombPrefab;
    public GameObject jumpEffectPrefab;
    public GameObject fallEffectPrefab;
    public GameObject runEffectPrefab;

    public LayerMask whatIsGround;
    public LayerMask whatIsExitDoor;
    public Vector2 defaultKnockbackAngle = Vector2.zero;

    public EventManager eventManager;
    public Core Core { get; private set; }
    #endregion

    //Initialize Core
    private void Awake() {
        Core = GetComponentInChildren<Core>();
    }
    //Initialize default settings and fetch objects
    void Start() {
        exitDoor = GameObject.FindGameObjectWithTag("ExitDoor").GetComponent<Animator>();
        bombsRefreshTimeLeft = bombsRefreshTime;
        amountOfBombsLeft = amountOfBombs;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        anim.SetBool("isHidden", true);
        amountOfJumpsLeft = amountOfJumps;
        healthAmount = healthAmountMax;
    }
    //Handles all the logic
    void Update() {
        CheckInput();
        CheckMovementDirection();
        UpdateAnimations();
        CheckIfCanJump();
        RefreshBombsAmount();
        enteredLevel = GameManager.Instance.GameStarted;
    }
    //Handles all physics and detections
    private void FixedUpdate() {
        ApplyMovement();
        CheckSurroundings();
        CheckKnockback();
    }
    //Checks if player is grounded or near the exit door
    private void CheckSurroundings() {
        if (rb.velocity.y <= 0.01f) {
            isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
            isAtExitDoor = Physics2D.OverlapCircle(exitDoorCheck.position, exitDoorCheckRadius, whatIsExitDoor);
            if (isGrounded) {
                isHit = false;
            } else {
                isJumping = true;
            }
        }
    }
    //Checks if the player can jump
    private void CheckIfCanJump() {
        if (isGrounded && rb.velocity.y <= 0) {
            amountOfJumpsLeft = amountOfJumps;
            if (isJumping) {
                Instantiate(fallEffectPrefab, fallEffectPosition.position, Quaternion.identity);
            }
            isJumping = false;
        }


        if (amountOfJumpsLeft <= 0) {
            canJump = false;
        } else {
            canJump = true;
        }
    }
    //Refreshs the amounts of bombs available, updates it with the UIManager
    private void RefreshBombsAmount() {
        if (amountOfBombsLeft < amountOfBombs) {
            bombsRefreshTimeLeft -= Time.deltaTime;
            UIManager.Instance.UpdateBombReloadTime((bombsRefreshTime - bombsRefreshTimeLeft) / bombsRefreshTime);
            if (bombsRefreshTimeLeft < 0 && amountOfBombsLeft < amountOfBombs) {
                bombsRefreshTimeLeft = bombsRefreshTime;
                amountOfBombsLeft++;
                UIManager.Instance.UpdateBombAmount(amountOfBombsLeft);
            }
        } else {
            UIManager.Instance.UpdateBombReloadTime(1f);
        }
    }
    //Handles all input detection and functions
    private void CheckInput() {
        if (Input.GetKey(KeybindManager.Instance.Keybinds["Pause"])) {
            GameManager.Instance.OpenOptionsMenu();
        }
        if (isAlive && canMove && enteredLevel && !exitingLevel) {
            if (Input.GetKey(KeybindManager.Instance.Keybinds["Left"])) {
                movementInputDirection = -1;
            } else if (Input.GetKey(KeybindManager.Instance.Keybinds["Right"])) {
                movementInputDirection = 1;
            } else {
                movementInputDirection = 0;
            }

            if (Input.GetKeyDown(KeybindManager.Instance.Keybinds["Jump"])) {
                SoundManager.Instance.PlayPlayerJump();
                Jump();
            }

            if (Input.GetKeyUp(KeybindManager.Instance.Keybinds["Jump"])) {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * variableJumpHeightMultiplier);
            }

            if (Input.GetKeyDown(KeybindManager.Instance.Keybinds["Bomb"])) {
                if (isAtExitDoor) {
                    anim.SetTrigger("doorIn");
                    exitDoor.SetTrigger("open");
                    exitingLevel = true;
                    rb.velocity = Vector2.zero;
                    SoundManager.Instance.PlayDoorOpen();
                    Invoke("ExitLevel", 0.75f);
                    return;
                }
                if (amountOfBombsLeft > 0) {
                    holdDownStartTime = Time.time;
                    holdingBomb = true;
                }
            }

            if (Input.GetKeyUp(KeybindManager.Instance.Keybinds["Bomb"])) {
                if (holdingBomb) {
                    PlaceBomb(holdDownTime);
                    holdingBomb = false;
                }
            }
        }

    }
    //Loads next scene when player enters a door
    private void ExitLevel() {
        LevelLoader.Instance.LoadNextScene();
    }
    //Handles the movement direction and effects

    private void CheckMovementDirection() {
        if (isFacingRight && movementInputDirection < 0) {
            Flip();
        } else if (!isFacingRight && movementInputDirection > 0) {
            Flip();
        }

        if (Mathf.Abs(rb.velocity.x) >= 0.01f) {
            if (!isWalking) {
                Instantiate(runEffectPrefab, runEffectPosition.position, transform.rotation);
                walkTimeStarted = Time.time;
            }

            isWalking = true;
        } else {
            isWalking = false;
        }
    }
    //Applies the movement for when the player can move
    private void ApplyMovement() {
        if (!isAlive && isGrounded && !isKnockbackActive || exitingLevel) {
            rb.velocity = Vector2.zero;
            if (exitingLevel) {
                return;
            }
            SoundManager.Instance.PlayPlayerDead();
            return;
        }

        if (canMove && isAlive)
            rb.velocity = new Vector2(movementSpeed * movementInputDirection, rb.velocity.y);
    }
    //Place or throws the bomb
    private void PlaceBomb(float holdDownTime) {
        if (amountOfBombsLeft > 0) {
            BombController bombPlaced = Instantiate(bombPrefab, bombPlacement.position, Quaternion.identity).GetComponent<BombController>();
            amountOfBombsLeft--;
            UIManager.Instance.UpdateBombAmount(amountOfBombsLeft);

            if (holdDownTime >= minHoldDownTime) {
                holdBarObject.BombReleased();
                bombPlaced.LaunchBomb(CalculateHoldDownForce(holdDownTime), isFacingRight ? 1 : -1);
            }

        }
    }
    //Calculates throw force with the amount or time the throw button was held
    private float CalculateHoldDownForce(float holdTime) {
        var logId = "CalculateHoldDownForce";
        float holdTimeNormalized = Mathf.Clamp01(holdTime / maxHoldDownTime);
        logd(logId, "HoldTime="+holdTime+" HoldTimeNormalized="+holdTimeNormalized);
        return holdTimeNormalized * maxBombForce;
    }
    //Flips the player
    private void Flip() {
        isFacingRight = !isFacingRight;
        transform.Rotate(0.0f, 180f, 0f);
    }
    //Vertical jump
    private void Jump() {
        if (canJump) {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            isGrounded = false;
            isJumping = true;
            amountOfJumpsLeft--;
            Instantiate(jumpEffectPrefab, jumpEffectPosition.position, Quaternion.identity);
        }
    }
    //Updates all the animations
    private void UpdateAnimations() {
        anim.SetBool("walking", isWalking);
        anim.SetBool("isGrounded", isGrounded);
        anim.SetBool("isKnockback", isKnockbackActive);
        anim.SetBool("isHit", isHit);
        anim.SetBool("isAlive", isAlive);
        anim.SetBool("canMove", canMove);
        anim.SetFloat("yVelocity", rb.velocity.y);
        anim.SetBool("isHidden", !enteredLevel);
        HandleBombBarAnimations();
        HandleRunningEffect();
    }
    //Handles the bomb force bar animation
    private void HandleBombBarAnimations() {
        if (holdingBomb) {
            holdDownTime = Time.time - holdDownStartTime;
            if (holdDownTime >= minHoldDownTime) {
                holdBarObject.EnableEffect();
            }
        }
    }
    //Handles the running animation
    private void HandleRunningEffect() {
        if (isWalking && isGrounded && isAlive) {
            walkTime = Time.time - walkTimeStarted;
            if (walkTime >= walkEffectCd) {
                Instantiate(runEffectPrefab, runEffectPosition.position, transform.rotation);
                walkTimeStarted = Time.time;

            }
        }
    }
    //For debugging purposes
    private void OnDrawGizmos() {
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);
    }
    //Handles the knockback applied to the player, either from bombs or pirate
    public void Knockback(float knockbackStrength, Vector2 knockbackOriginPosition) {
        var logId = "Knockback";
        if (!isKnockbackActive && !exitingLevel) {

            Vector2 position = transform.position;
            var xDifference = position.x - knockbackOriginPosition.x;
            var yDifference = position.y - knockbackOriginPosition.y;
            Vector2 angle = new Vector2(xDifference, yDifference).normalized;

            if (!rb.bodyType.Equals(2)) {

                logd(logId, "TransformPos="+position+" KnockbackPos="+knockbackOriginPosition+" xDifference="+xDifference+" yDifference="+yDifference);
                if (TargetAndBombHaveSameHeight(position, knockbackOriginPosition)) {
                    rbVelocity = new Vector2(angle.x * knockbackStrength * xKnockbackMultiplier,
                        angle.y * knockbackStrength * yKnockbackMultiplier);
                } else {
                    rbVelocity = new Vector2(angle.x * knockbackStrength * xKnockbackMultiplier,
                        angle.y * knockbackStrength);
                }
                rb.isKinematic = true;
                ApplyKnockback();
                rb.isKinematic = false;
            }
            holdBarObject.BombReleased();
        }
    }
    //Returns if the player is alive
    public bool IsAlive() {
        return isAlive;
    }
    //Applies the knockback to the rigidbody
    public void ApplyKnockback() {
        var logId = "ApplyKnockback";
        logd(logId, "Applying RbVelocity="+rbVelocity);
        rb.velocity = Vector2.zero;
        isKnockbackActive = true;
        knockbackStartTime = Time.time;
        rb.velocity = rbVelocity;
        canMove = false;
    }
    //Damage the player
    public void Damage(int amount) {
        var logId = "Damage";
        SoundManager.Instance.PlayPlayerHit();
        isHit = true;
        healthAmount -= amount;
        UIManager.Instance.UpdatePlayerHealth(healthAmount);
        logd(logId, "Taking "+amount+" damage => Health="+healthAmount);
        if (healthAmount <= 0f) {
            GameManager.Instance.GameOver();
            eventManager.BroadCastPlayerDeath();
            isAlive = false;
            canMove = false;
        }
        holdBarObject.BombReleased();
    }
    private void CheckKnockback() {
        if (Time.time >= knockbackStartTime + knockbackDuration && isKnockbackActive && isGrounded) {
            isKnockbackActive = false;
            canMove = true;
        }
    }

    private bool TargetAndBombHaveSameHeight(Vector2 targetPosition, Vector2 originPosition) {
        return originPosition.y + 1 > targetPosition.y;
    }

    public void Knockback(int facingDirection) {
        if (!isKnockbackActive && !exitingLevel) {

            if (!rb.bodyType.Equals(2)) {

                rbVelocity = new Vector2(defaultKnockbackAngle.x * facingDirection, defaultKnockbackAngle.y);
                rb.velocity = rbVelocity;

                rb.isKinematic = true;
                ApplyKnockback();
                rb.isKinematic = false;
            }
            holdBarObject.BombReleased();
        }
    }
}
