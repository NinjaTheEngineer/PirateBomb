﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using NinjaTools;
public class Knockbackable : NinjaMonoBehaviour, IKnockbackable {
    public float xDetonationPowerAmp = 1f;
    public float yDetonationPowerAmp = 1f;

    public Rigidbody2D rb;
    //Handle knockback

    public void Knockback(float knockbackStrength, Vector2 bombPosition) {
        var logId = "Knockback";
        Vector2 angle = new Vector2(bombPosition.x - transform.position.x, bombPosition.y - transform.position.y).normalized;
        Vector2 rbVelocity;

        if (!rb.bodyType.Equals(2)) {
            if (TargetAndBombHaveSameHeight(transform.position, bombPosition)) {
                rbVelocity = new Vector2(angle.x * -1 * knockbackStrength * xDetonationPowerAmp,
                    angle.y * knockbackStrength * yDetonationPowerAmp);
            } else {
                rbVelocity = new Vector2(angle.x * -1 * knockbackStrength * xDetonationPowerAmp,
                    angle.y * knockbackStrength * yDetonationPowerAmp);
            }
            logd(logId, "Angle : " + rbVelocity);

            rb.velocity = rbVelocity;
        }
    }

    public void Knockback(float knockbackStrength, int facingDirection) {
        throw new System.NotImplementedException();
    }
    //Check if target at the same height
    private bool TargetAndBombHaveSameHeight(Vector2 targetPosition, Vector2 bombPosition) {
        return bombPosition.y + 1 > targetPosition.y;
    }
}
