﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using NinjaTools;

public class EnvironmentObject : NinjaMonoBehaviour, IKnockbackable {
    public float weight;

    public float xKnockbackMultiplier;
    public float yKnockbackMultiplier;

    public Rigidbody2D rb { get; private set; }
    private Vector2 rbVelocity;
    //Get rb for the object
    private void Start() {
        rb = GetComponent<Rigidbody2D>();
    }
    //Handle knockback
    public void Knockback(float knockbackStrength, Vector2 knockbackPosition) {
        var logId = "Knockback";
        Vector2 position = transform.position;
        Vector2 angle = new Vector2(position.x - knockbackPosition.x, position.y - knockbackPosition.y).normalized;

        if (!rb.bodyType.Equals(2)) {

            logd(logId, "transform.position.x > bombPosition.x: " + (position.x > knockbackPosition.x) + " _ transform.position.x - " + position.x + " _bombPosition.x - " + knockbackPosition.x);
            if (TargetAndBombHaveSameHeight(position, knockbackPosition)) {
                rbVelocity = new Vector2(angle.x * knockbackStrength * xKnockbackMultiplier,
                    angle.y * knockbackStrength * yKnockbackMultiplier);
            } else {
                rbVelocity = new Vector2(angle.x * knockbackStrength * xKnockbackMultiplier,
                    angle.y * knockbackStrength);
            }

            rb.velocity = rbVelocity;
        }
    }
    //Check if at same position as target
    private bool TargetAndBombHaveSameHeight(Vector2 targetPosition, Vector2 originPosition) {
        return originPosition.y + 1 > targetPosition.y;
    }
}
