﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NinjaTools;

public class Core : NinjaMonoBehaviour {
    [SerializeField] GameObject visualGO;
    public GameObject Visual => visualGO != null ? visualGO : null;
    public Movement Movement { get; private set; }
    public Combat Combat { get; private set; }
    public CollisionSenses CollisionSenses { get; private set; }
    //Initialize components
    private void Awake() {
        var logId = "Awake";
        Movement = GetComponentInChildren<Movement>();
        Combat = GetComponentInChildren<Combat>();
        CollisionSenses = GetComponentInChildren<CollisionSenses>();

        if (!Movement || !Combat) {
            loge(logId, "Movement="+Movement.logf()+" Combat="+Combat.logf()+" => no-op");
        }
    }
    //Logic update, same has an Update of Monobehaviour
    public void LogicUpdate() {
        Movement.LogicUpdate();
        Combat.LogicUpdate();
    }
    public override string ToString() => "Core from "+gameObject.name+" Visual="+Visual;
}

