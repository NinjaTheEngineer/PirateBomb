﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NinjaTools;

public class CoreComponent : NinjaMonoBehaviour {
    protected Core core;

    protected GameObject visualGameObject;

    protected virtual void Awake() {
        var logId = "Awake";
        core = transform.parent.GetComponent<Core>();
        visualGameObject = core.Visual;

        if (core == null) {
            loge(logId, "Core="+core.logf());
        }
    }
}
