﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using NinjaTools;

public class PushObjects : NinjaMonoBehaviour {
    private EnvironmentObject parentObject;
    private void Start() {
        parentObject = GetComponentInParent<EnvironmentObject>();
    }
    //Push objects by enemies player or bombs
    private void OnTriggerEnter2D(Collider2D collision) {
        var logId = "OnTriggerEnter2D";
        GameObject collider = collision.gameObject;
        if (collider.CompareTag("Enemy") || collider.CompareTag("Player") || collider.CompareTag("Bomb")) {
            Rigidbody2D colliderRb = collider.GetComponent<Rigidbody2D>();
            if (colliderRb != null) {
                Vector2 pushVelocity = colliderRb.velocity / parentObject.weight;
                if (pushVelocity.y <= 0.01f && pushVelocity.y >= -0.01f)
                    pushVelocity.y = 0.75f;
                logd(logId,"PushVelocity="+pushVelocity);
                parentObject.rb.velocity = pushVelocity;
            }
        }
    }
}
