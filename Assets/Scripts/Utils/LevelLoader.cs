using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour {
    public Animator transition;
    public float transitionTime = 0.45f;

    private static LevelLoader instance;
    //Singleton
    public static LevelLoader Instance {
        get {
            if (instance == null) {
                instance = FindObjectOfType<LevelLoader>();
            }
            return instance;
        }
    }

    //Load next scene
    public void LoadNextScene() {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }
    //Load main menu
    public void LoadMainMenu() {
        StartCoroutine(LoadLevel(0));
    }
    //Restart level
    public void RestartLevel() {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex));
    }
    //Load entered level
    IEnumerator LoadLevel(int levelIndex) {
        transition = GetComponentInChildren<Animator>();
        transition.SetTrigger("Start");

        SoundManager.Instance.PauseBackgroundMusic();
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(levelIndex);
    }

}

