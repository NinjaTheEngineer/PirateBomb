﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializableEffect : MonoBehaviour {
    //Activates any effect
    public void ActivateEffect() {
        gameObject.SetActive(true);
    }
    //Deactivates any effect
    public void DeactivateEffect() {
        gameObject.SetActive(false);
        Destroy(gameObject);
    }
}
