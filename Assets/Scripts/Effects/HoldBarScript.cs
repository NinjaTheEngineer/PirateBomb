﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldBarScript : MonoBehaviour {
    public Animator anim;
    private bool isEnabled = false;
    //Initialize variables
    private void Start() {
        anim = GetComponent<Animator>();
        gameObject.SetActive(false);
    }
    //Update logic, the animations here
    private void Update() {
        UpdateAnimations();
    }
    //Update animations
    private void UpdateAnimations() {
        anim.SetBool("released", !isEnabled);
    }
    //Disable effect when bomb released
    public void BombReleased() {
        DisableEffect();
    }
    //Enable the bomb bar effect
    public void EnableEffect() {
        if (!isEnabled) {
            gameObject.SetActive(true);
            isEnabled = true;
        }
    }
    //Set the full charged effect
    public void FullHold() {
        anim.SetBool("full", true);
    }
    //Disable the bomb bar effect
    public void DisableEffect() {
        if (isEnabled) {
            gameObject.SetActive(false);
            isEnabled = false;
        }
    }
}
